/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.random.end;

import java.io.RandomAccessFile;

/**
 *
 * @author PC
 */
public abstract class Base<T> implements IRandom{
        
    
    protected final RandomAccessFile raf;
    
    private T dato;
        
    public Base(String rutaArchivo)throws Exception{
        raf = new RandomAccessFile(rutaArchivo, "rw");
    }

    public T getDato() {
        return dato;
    }

    public void setDato(T dato) {
        this.dato = dato;
    }
    
    
    
}
