package com.random.end;

import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author PC
 */
public class Main {

    public static void main(String[] args) {
        int parametro = menu();

        if (parametro == 1) {
            try{
            Vehiculo v = new Vehiculo("C:\\Users\\PC\\Documents\\vehiculo.txt");
            v.setDato(nuevoVehiculo());
            if (v.insertar()) {
               System.out.println("Correcto");
            } else 
            {
                System.out.println("Error");
           }
         
            }catch (Exception e){
                System.out.println(e);
            }
        } else 
            if (parametro ==2) {
              try{
            Vehiculo v = new Vehiculo("C:\\Users\\PC\\Documents\\vehiculo.txt");
                if (v.editar()) {
                System.out.println("Edito correctamente");
                imprimirConsola();
            }
            else
            {
                System.out.println("Editó mal");
            }
              }catch (Exception e){
                System.out.println(e);
            }
        }

    }

    public static int menu() {
        Scanner lector = new Scanner(System.in);
        int opcion = 0;
        do {
            System.out.println("Digitar la opción ");
            System.out.println("1. Nuevo Registro");
            System.out.println("2. Visualizar Registro");
            System.out.println("3. Editar Registro");
            System.out.println("4. Inactivar Registro");
            System.out.println("5. Salir");
            try {
                opcion = lector.nextInt();
            } catch (Exception e) {
                lector.next();
                System.out.println("Solo se aceptan números");
                opcion = 0;
            }

        } while (opcion <= 0 || opcion > 5);

        return opcion;
    }

    public static com.bean.end.Vehiculo nuevoVehiculo() {
        com.bean.end.Vehiculo dato = new com.bean.end.Vehiculo();
        Scanner leer = new Scanner(System.in);
        //Falta validación de solo números
        System.out.println("Digite placa de vehiculo: ");
        dato.setPlaca(leer.nextInt());
        // falta validación solo de letras
        System.out.println("Digite color de vehiculo: ");
        dato.setColor(leer.next());
        System.out.println("Digite Marca del vehículo: ");
        dato.setMarca(leer.next());
        System.out.println("Digite Modelo: ");
        dato.setModelo(leer.next());
        //Falta validación de solo numeros
        System.out.println("Digite Cantidad pasajeros: ");
        dato.setCantidadPasajeros(leer.nextInt());
        return dato;
    }
    
    public static void imprimirConsola(){
        try{
        Vehiculo v = new Vehiculo("C:\\Users\\PC\\Documents\\vehiculo.txt");
        ArrayList<com.bean.end.Vehiculo> datos= v.getDatos();
            for (com.bean.end.Vehiculo dato : datos) {
                System.out.println(dato.getPlaca());
                System.out.println(dato.getColor());
                System.out.println(dato.getMarca());
                System.out.println(dato.getModelo());
                System.out.println(dato.getCantidadPasajeros());
                System.out.println("-----------------------------------");
            }
        } catch (Exception e){
            System.out.println(e);
        }
    }
    

}
